# Matrix Multiplication with MapReduce

### NTHU-Introduction to Massive Data Analysis


Problem Describtion: [Demand](https://gitlab.com/tzuyangw/matrix-multiplication-with-mapreduce/blob/master/Demand.pdf)

Requirement: 
1. Python3
2. Pyspark
3. JDK
4. Apache-spark

Input file:
1. Test input: [test.txt](https://gitlab.com/tzuyangw/matrix-multiplication-with-mapreduce/blob/master/test.txt)
2. Formal input: [500input.txt](https://gitlab.com/tzuyangw/matrix-multiplication-with-mapreduce/blob/master/500input.txt)

Please put the input file and code file in the same directory.
You can try the Test.txt first which contain two 4*4 matric data.
If all your environment are set up completely, it will output a result.txt file.


For more detail, you can see the [Report.pdf](https://gitlab.com/tzuyangw/matrix-multiplication-with-mapreduce/blob/master/Assignment_Report.pdf) to realize how it works.



