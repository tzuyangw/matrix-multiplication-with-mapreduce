from pyspark import SparkConf, SparkContext

def mapper_row_element(line):
    elements = line.split("\n")
    maplist = []
    for element in elements:
        indice = element.split(',')
        maplist.append((indice[2], (indice[1], int(indice[3]))))
    return maplist

def mapper_column_element(line):
    elements = line.split("\n")
    maplist = []
    for element in elements:
        indice = element.split(',')
        maplist.append((indice[1], (indice[2], int(indice[3]))))
    return maplist

def reducer_add_result(x, y):
    return x+y


conf = SparkConf().setMaster("local").setAppName("MatrixMultiply")
sc = SparkContext(conf=conf)

# Read the data from .txt file and split the data into two types(row and column)
row_data = sc.textFile("500input.txt").filter(lambda d : d.find('M') > -1)
column_data = sc.textFile("500input.txt").filter(lambda d : d.find('N') > -1)

# Mapping the raw data
row_rdd = row_data.flatMap(mapper_row_element)
column_rdd = column_data.flatMap(mapper_column_element)

# Map data to produce ((i, k), value) element
result_of_join = row_rdd.join(column_rdd).values().map(lambda e: ((e[0][0], e[1][0]), e[0][1] * e[1][1]))
result_of_reduce = result_of_join.reduceByKey(reducer_add_result)
sorted_output = result_of_reduce.sortBy(lambda r: (r[0][0], r[0][1])).collect()
# print(sorted_output)

with open('result.txt','w', encoding='UTF-8') as file:
	for element in sorted_output:
		file.write(element[0][0]+ " " + element[0][1] + " " + str(element[1]))
		file.write('\n')
print("End")

sc.stop()
